# Trustline
## Installation

If you using mac, install the latest version of elixir 
`brew install elixir` OR `sudo port install elixir`

FOR windows or linux - https://elixir-lang.org/install.html

Pull the latest version `git clone https://bitbucket.org/brangi/trustline.git`


## Compile

cd `trustline`

run `mix escript.build` (you might not need to recompile because the executable is included in this repository, but just in case)


## RUN

```elixir
./trustline --portHost=5555  --portTrustee=4444
Welcome to your trustline!
balance - Check your balance
exit - Exit Trustline
pay - Pay your trust party - separate cmd word and the amount with a space. Eg: 'pay 10'


```

